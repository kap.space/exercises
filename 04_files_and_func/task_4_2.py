"""
Задание 4.2
Напишите функцию, которая проверяет является ло передаваемый ей год високосным или нет.
      'На вход подается год. 
      Функция должна возвращать:
        True - високосный, 
        False - обычный'
"""
def leap(year: int):
    if year % 4 == 0:
        if year % 400 == 0:
            return True
        return False if year % 100 == 0 else True
    return False

years = [2000, 2100, 2004, 1900, 1996]
for y in years:
    print(leap(y))
