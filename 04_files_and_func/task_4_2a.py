"""
Задание 4.2a
Напишите функцию, которая вычисляет количество дней в году (учитывать високосный год).
      На вход подается год.
      Функция возвращает кол-во дней
Используйте функцию из предыдущего задания, для проверки является ли год високосным.
"""
def leap(year: int):
    if year % 4 == 0:
        if year % 400 == 0:
            return True
        return False if year % 100 == 0 else True
    return False

def getYearDays(year: int):
    return 366 if leap(year) else 365

years = [2000, 2100, 2004, 1900, 1996]
for y in years:
    print(getYearDays(y))
