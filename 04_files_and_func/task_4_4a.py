"""
Здание 4.4a
Cкопируйте функции написаные в предыдущем задании
Написать функции:

validate_operator(operator)
    функция проверяет, что operator равен допустимым значениям
        допустимые значения: + - * /
    Если оператор принимает допустимое значение:
        возвращается True
    Если оператор НЕ принимает допустимое значение:
        выводится строка "Неизвестный оператор"
        И возвращается False

validate_value(var)
    Функция проверяет что значение val либо int либо float
    Если все верно - возвращается True
    Если оператор равен "/" а второй операнд равен нулю:
        Выводится строка: "На ноль делить нельзя!"
        И возвращается - False
    В остальных случаях:
        Выводится строка: "Неподдерживаемый тип опреранда"
        И возвращается - False
    
calculator(v1 :int or float , operator :str, v2 :int or float)

При запуске функции:
    Проверяются оператор и операнды функциями:
        validate_operator
        validate_value
В зависимости от оператора выплнить соответствующую функцию из предыдучего задания
    Результат выполнения записать в переменную result и вернуть в конце
"""
availableTypes = [int, float]
availableOperator = ['+', '-', '*', '/']

def plus_args(v1, v2):
    print(f'Операция - сложение: {v1} + {v2}')
    return v1 + v2

def minus_args(v1, v2):
    print(f'Операция - вычетание: {v1} - {v2}')
    return v1 - v2

def multi_args(v1, v2):
    print(f'Операция - умножение: {v1} * {v2}')
    return v1 * v2

def div_args(v1, v2):
    print(f'Операция - деление: {v1} / {v2}')
    return v1 / v2

def validate_operator(operator):
    if operator in availableOperator:
        return True
    print('Неизвестный оператор')
    return False

OPERATIONS = {'+': plus_args, '-': minus_args, '*': multi_args, '/': div_args}

def validate_value(val, operator=''):
    if type(val) in availableTypes:
        if operator == '/' and val == 0:
            print('На ноль делить нельзя!')
            return False
        return True
    print('Неподдерживаемый тип опреранда')
    return False

def calculator(v1, operator: str, v2):
    result = None
    if validate_operator(operator) and validate_value(v1) and validate_value(v2, operator):
        result = OPERATIONS[operator](v1, v2)
    return result

print(calculator(2.0, '/', 0.0))
print(calculator(10, '/', 2.0))
print(calculator(5, '+', 9))
print(calculator(5, 'mul', 9))
print(calculator(5, '-', '9'))
