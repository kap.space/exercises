# -*- coding: utf-8 -*-
'''
Задание 4.1

Обработать строки из файла servers.txt
и вывести информацию по каждому серверу в таком виде:

server:            qa_test1
ip:                10.0.13.3
switch_port:       FastEthernet0/0
uptime:            3d20h

Ограничение: Все задания надо выполнять используя только пройденные темы.
'''
f = open('servers.txt', 'r')
for line in f:
    if line.endswith('\n'):
        line = line[:-1]
    line = line.replace(',', '')
    fields = [fld for fld in line.split(' ') if fld != '']
    print(f'''
server:            {fields[1]}
ip:                {fields[4]}
switch_port:       {fields[6]}
uptime:            {fields[5]}''')
