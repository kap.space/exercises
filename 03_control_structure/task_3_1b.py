'''
Задание 3.1b
Скопировать код из предыдущего задания


Допишите код, чтобы:
    - после вывода результата код запускался заново и опять запрашивал ввод выражения
    - при вводе пустрой строки программа останавливалась

'''
while True:
    calc = input("Введите выражение: ")
    if calc == '':
        break
    if '+' in calc:
        ab = calc.split('+')
        print(f"Результат сложения: {int(ab[0].strip()) + int(ab[1].strip())}")
    elif '-' in calc:
        ab = calc.split('-')
        print(f"Результат вычетания: {int(ab[0].strip()) - int(ab[1].strip())}")
    elif '*' in calc:
        ab = calc.split('*')
        print(f"Результат умножения: {int(ab[0].strip()) * int(ab[1].strip())}")
    elif '/' in calc:
        ab = calc.split('/')
        print(f"Результат вычетания: {int(ab[0].strip()) / int(ab[1].strip())}")
    else:
        print("Невалидное выражение")