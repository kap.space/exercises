# -*- coding: utf-8 -*-
'''
Задание 1.8

Дано:
student - кол-во школьников
apple - кол-во яблок

Школьники делят между собой  поровну яблоки из корзины
Оставшиеся яблоки остаются в корзине
Подсчитать сколько яблок достанется одному школьнику

Вариант для продвинутых:
Дополнительно написать код, что число школьников 
и яблок - задается с клавиатуры пользователем
'''
def input_natural_number(message):
    while True:
        s = input(message)
        if s.isdigit() and int(s) > 0:
            return int(s)
        print("Ввод только положительное натуральное число")

student = 7
apple = 59
print(apple // student)

student = input_natural_number("Количество студентов: ")
apple = input_natural_number("Количество яблок: ")
print(apple // student)