# -*- coding: utf-8 -*-
'''
Задание 1.7

Дано число n

Подсчитать и вывесети на экран сумму эго цифр

Вариант для продвинутых:
Дополнительно написать код, что 5ти значное число n - задается с клавиатуры пользователем
'''

def summarize(intstr):
    res = 0
    for i in range(0, len(intstr)):
        res += int(intstr[i])
    return res

n = 65142
print(summarize(str(n)))

s = ''
while True:
    s = input("Enter five digital of number: ")
    if len(s) == 5 and s.isdigit():
        break
    else:
        print("Only number from five digital")
print(summarize(s))
